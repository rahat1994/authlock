<p align="center"><a href="https://laravel.com" target="_blank"></a></p>

<img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400">

## AuthLock - A simple credential management tool.

### Technologies Used

-   Frontend
    -   Livewire
    -   alpinejs
    -   Bootstrap 5
-   Backend
    -   Laravel framework
    -   Mysql
    -   NGINX web server

### Requirements

-   PHP > 7.3
-   MYSQL 8.0.30

## Copy functionality wont work if theapp is not served over SSL.

### Installation steps

1. Git clone the repository
2. cd into that repository
3. install the dependecies

    `composer install`

4. create .env file

    `cp .env.example .env`

5. Generate app key

    `php artisan key:generate`

6. Make sure you have created the database and database user and run the migrations. fill the .env file created in step 4 with the new info.

    `php artisan migrate`

7. Populate necessary data using seeder

    `php artisan db:seed --class=NecessaryDataSeeder`

8. Serve the application

    `php artisan serve`
