<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NecessaryDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('types')->insert([
            'name' => 'Card'
        ]);

        DB::table('types')->insert([
            'name' => 'Login'
        ]);

        DB::table('types')->insert([
            'name' => 'Security Note'
        ]);
    }
}
