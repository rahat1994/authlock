<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->foreignId('type_id')->constrained('types');
            $table->foreignId('organization_id')->constrained('organizations');
            $table->foreignId('folder_id')->constrained('folders');
            $table->foreignId('collection_id')->constrained('collections');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('type_id');
            $table->dropColumn('organization_id');
            $table->dropColumn('folder_id');
            $table->dropColumn('collection_id');
        });
    }
}
