<?php

use App\Http\Controllers\Data\ItemDataController;
use App\Http\Controllers\User\UsersController;
use App\Http\Livewire\Tools\PasswordGEnerator;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/tools', PasswordGEnerator::class)->middleware(['auth'])->name('tools');

Route::post('item/export', [ItemDataController::class, 'export'])->middleware(['auth'])->name('items.export');
Route::post('item/import', [ItemDataController::class, 'import'])->middleware(['auth'])->name('items.import');

Route::get('modal_test', function () {
    return view('modal_test');
})->name('modal_test');

require __DIR__ . '/auth.php';