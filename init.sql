-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table authlock.collections: ~2 rows (approximately)
INSERT INTO `collections` (`id`, `name`, `organization_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 'Default Collection', 6, 11, '2023-03-23 12:34:20', '2023-03-23 12:34:20'),
	(2, 'Default Collection', 7, 12, '2023-03-23 12:38:21', '2023-03-23 12:38:21'),
	(3, 'Default Collection', 8, 13, '2023-03-24 00:26:32', '2023-03-24 00:26:32');

-- Dumping data for table authlock.failed_jobs: ~0 rows (approximately)

-- Dumping data for table authlock.folders: ~16 rows (approximately)
INSERT INTO `folders` (`id`, `name`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 'New Folder', 4, NULL, NULL),
	(2, 'My important folder', 4, '2023-03-22 13:06:52', '2023-03-22 13:06:52'),
	(3, 'total folder', 4, '2023-03-22 13:07:00', '2023-03-22 13:07:00'),
	(4, 'geekphone', 4, '2023-03-22 13:07:18', '2023-03-22 13:07:18'),
	(5, 'Default Folder', 9, '2023-03-23 12:21:42', '2023-03-23 12:21:42'),
	(6, 'Default Folder', 11, '2023-03-23 12:34:20', '2023-03-23 12:34:20'),
	(7, 'Default Folder', 12, '2023-03-23 12:38:21', '2023-03-23 12:38:21'),
	(8, 'My importnant folder', 12, '2023-03-23 14:04:07', '2023-03-23 14:04:07'),
	(9, 'Default Folder', 13, '2023-03-24 00:26:32', '2023-03-24 00:26:32'),
	(10, 'My folder', 13, '2023-03-24 02:55:24', '2023-03-24 02:55:24'),
	(11, 'your folder', 13, '2023-03-24 02:56:08', '2023-03-24 02:56:08'),
	(12, 'Test New Folder', 13, '2023-03-24 02:59:11', '2023-04-12 15:43:57'),
	(13, 'pages gmail', 13, '2023-03-28 14:15:08', '2023-03-28 14:15:08'),
	(14, 'High table Important', 13, '2023-03-29 11:25:11', '2023-03-29 11:25:11'),
	(15, 'baron admins', 13, '2023-03-29 12:32:33', '2023-03-29 12:32:33'),
	(16, 'phillips important', 13, '2023-03-29 13:23:12', '2023-03-29 13:23:12');

-- Dumping data for table authlock.items: ~11 rows (approximately)
INSERT INTO `items` (`id`, `name`, `username`, `password`, `key`, `note`, `master_pass_secured`, `is_favourite`, `in_trash`, `deleted`, `created_at`, `updated_at`, `type_id`, `organization_id`, `folder_id`, `collection_id`, `user_id`, `login_url`) VALUES
	(58, 'Zephania Shannon', 'dubif', 'def5020065fec48146a2f397565c72045465af41dea2ef58bf2d0dcc2fe83910f72a295164064b71abaa50e3a8d78ba79c85936f117e0a3a4d965aa683ea4d385884d5d756712a5878db1a7b0f6f3002a225771b3a6b657747dbe8', 'def00000510d67f383a809d27258f45a1eb002eb3aa168cb644282baeb34cd1bcf6872315f39c64e52127d1336d87f7f4d5316f44652aed8fa91b0dc33553b911c8c5f85', 'Veniam quisquam con', 1, 1, 0, 0, '2023-04-14 03:21:32', '2023-04-14 16:16:57', 1, 13, 12, 3, 13, 'Id Nam perferendis p'),
	(59, 'Blythe Wick', 'fydahi', 'def50200b38b342a181b692a6171f99cd666580690d7c5c017ba6efd5fac8f038ce50cc83cddb8b38a4b0c1eabe54108718f408723ce6dfbed0aade5f36863057d3813d86956c210e418364ce2e67d9a2323a123dea0896031cde005', 'def00000f5723e0bf4fdbcd2652d7337e5d3a2205bffc61e47d761fd2db0d09a26f6bb13b526c727153f5bbddda63dfb738f5aedf7cde956a55e341319cfc10a55588e1a', 'Autem dolores pariat', 1, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 2, 8, 11, 3, 13, 'gmail.com'),
	(60, 'Lynn Decker5', 'vapyju', 'def5020021358b4c0db51b28cb495ddc9f7d5cdfe937d9c91273a9e3a5a8fb53bbef9202444a8c195f293f4f89a0190d0b1b4e5d7152f36e8111d45e4b02ffdb8bc996a51fbfcf63a4689b8b648cbd6e7b216afa6df77a589dabde381556', 'def000001097e83925fe469a23d80c272308473fb5923a0557bf36ad5e73d5235903d5d8003ebc2e305e1fcf1191cdf093b267b1dd163f04a5f903964f0232629c0f2abb', 'Harum ut laudantium', 1, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 2, 8, 9, 3, 13, 'gmail.com'),
	(61, 'Melyssa wick', 'domex2', 'def50200d23fa1c694a7e944ab581cc1944980e5dffc812d6089a40f30e54496178cc3408a05ba73f8acbde98b7e63df16a36bde0ee8db389c063580e43dc0c5a83eea9550f7d104adc5d39ba5b132a487bc157339725a3efe515e4a10', 'def00000f0d45d6d13d04cebbae51c61da858c43ded2517770f73bda02ba6e7b8f36fce779db85eebcd58fad8f52fc2342b9bbb8843970e998a3e0e0830ede2fbe7c75d0', 'this sia note', 1, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 2, 8, 9, 3, 13, 'gmail.com'),
	(62, 'Haley Frye', 'mipikoqu', 'def50200ae3f527d0c0460e0295ef74bb0bfb8bdb9eabca8f7b73d5d148d6e2d1e5d35e3722342afab17a687f9f9077590bf619c6d4392e63aff098011c16770e6e430182175277689d5bbd0d62d4e663f77b2f8a82e3170072aa966a55a', 'def000000b75be55dcd7bdd03d9ee8d9096e86d37843ca4e1fda67243950eda8e9b4004105477df1d2e61461781fa80d6fb1c085046a1c36951fce0f76e80a5d0f40797e', 'Eum tempor est delec', 0, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 2, 8, 10, 3, 13, 'gmail.com'),
	(63, 'Carl Shields', 'walaporu', 'def50200076e1ac669a1fd9a5ecb6f80c519718f1995a5f683de5c58c0b3d753e4b1b6464ad7dc471c7c5e9a643f20363a5f1445d53a050ee06b41c443dad3fe6f2204d87aa4dd06955780b2e85a44d4f6dab9a2628c787037beae4003', 'def000007d6d7a64a0f93dd3b90cd4bb81baaa06f0452f72261c9412a7d7e826effd4af70959f9fd6cc7d20bd94efdde34e2a8b7e95cbe5861ed6f28d33d276380e05f4f', 'Vel sit at dolore et', 1, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 2, 8, 9, 3, 13, 'gmail.com'),
	(64, 'Wendy Neal', 'gybojebu', 'def50200e03bfd9e12ccd60440b6d04b32ce3701d703d8a801f8337b6e2b0def6072c88710926af29ea764f522a427ba31bafc863282c073ecf6f018207bacabecf4234a100750179c991fdff96606b4ab0cf49f9058d76b7fd503f0d7', 'def0000049c2188090d8fd22a337aecd7c6fab7d31a8d5abcc4629baf569e2ac59ae6cdecc95294363352ea18c2c662ce9c4b91e8f1c01c7665b29fa452dd7b3813724c1', 'Adipisicing nisi dol', 0, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 1, 8, 10, 3, 13, 'gmail.com'),
	(65, 'Freya Watson', 'gyhoceloco', 'def50200a7dee28422f154422c48710b7081ee7986169031fcbad6e376191349bb95149c83e28cbcb97b13850250711f5d7b5a37f53d08a9aefb591d13c9c2e01c413f7457d35497b6de96ffb2f26ac06294e1aeba4ab8e08a154916f7', 'def00000fd88a95bca4cbadb36fb31b79f141292c2a6642d0498ea4c68aff951dd12ae0b10d0288534d24b3d0eced5d15d8f7574a4b5638815e0ce4c191841f55164435f', 'Unde id fugiat do qu', 1, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 1, 8, 10, 3, 13, 'gmail.com'),
	(66, 'Ginger Nieves', 'rahat123', 'def5020091da08ee433cb18f5d8839158722faa59941ae69c311f5adc7a44a3e3b1397136e8a2f79170f3dc619d90abb0a42ba849e663ce818fdfbbf58ea8dc72454dfdda107a046c6d9f34a58f22cc9b0724c2c09ece3c6060801f738429f2bfa', 'def00000b855c0d8148036bcf0723a37fd778396b475c9ebe564f5e3232aa9a91e72b0cb8b448d1ac47168bc7494b6b5b92c9543bfdd92b42a003580db85fe007997e040', 'lokk out for fake ads', 1, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 2, 8, 9, 3, 13, 'gmail.com'),
	(67, 'Mason', 'naloqir', 'def502006c67109c027060a8784aca80483ecae7eb356168754f025c1b7cd57eaa9109ae9f03811d278faf37fb24bc4921427bda63e447073689a9d9c8612e1c787167352100db6a7714c518007e63b676dcf20b9e73d8bc0de177e0', 'def00000bdaff672c366308896b8faf84a11695d4053651208a2bd6324dae52d7550cbfb6d24ec4aeea5133d9c4af7b14965b1514a17854de03188e24218b7b54d52070d', 'Nemo illum dolorum ', 1, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 1, 8, 15, 3, 13, 'Laboris vero aute vo'),
	(68, 'Price Faulkner', 'rahat123', 'def50200d8d462152b9d39a201caf093df6418676b2807bdcb05078c055f58d83b219dab67d0c61d12a9a77c30d3a9faa8571044505bbfc5f9efa3a34f6124c505cab24ae6cc167e9ea2558a9772fd99dd787852207a8cc06fc6516c', 'def00000de88cc208e9b6acd3756015a08d53a15e7de3c7e85033aa651b3637b25a24224e11a944afd424cf43291396f9592924d87cd99137727d6663979e8bb731c78ac', 'this is a note.', 0, 0, 0, 0, '2023-04-14 16:16:26', '2023-04-14 16:16:26', 1, 8, 16, 3, 13, 'streamz.com');

-- Dumping data for table authlock.migrations: ~10 rows (approximately)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(5, '2023_03_21_065231_create_item_table', 2),
	(6, '2023_03_21_095532_create_types_table', 2),
	(7, '2023_03_21_095638_create_folders_table', 2),
	(8, '2023_03_21_095703_create_organizations_table', 2),
	(9, '2023_03_21_111438_create_collections_table', 2),
	(10, '2023_03_21_175128_add_columns_to_items_table', 3),
	(11, '2023_03_21_192126_add_column_user_id_to_organizations_table', 4),
	(12, '2023_03_22_100743_create_user_organizations_table', 5),
	(13, '2023_03_23_090616_add_column_to_items_table', 6),
	(14, '2023_03_23_091557_add_password_column_to_items_table', 6),
	(15, '2023_03_28_100456_add_login_url_column_to_items_table', 7),
	(16, '2023_04_10_093224_make_item_folder_id_nullable', 8),
	(17, '2023_04_13_171149_add_key_column_to_items_table', 8),
	(18, '2023_04_14_082432_make_items_table_password_filed_text', 9);

-- Dumping data for table authlock.organizations: ~16 rows (approximately)
INSERT INTO `organizations` (`id`, `name`, `billing_email`, `created_at`, `updated_at`, `user_id`) VALUES
	(1, 'Torres Bullock LLC', 'mekesu@mailinator.com', '2023-03-23 12:04:42', '2023-03-23 12:04:42', 5),
	(2, 'myjevun@mailinator.com', 'myjevun@mailinator.com', '2023-03-23 12:11:11', '2023-03-23 12:11:11', 7),
	(3, 'getul@mailinator.com', 'getul@mailinator.com', '2023-03-23 12:20:35', '2023-03-23 12:20:35', 8),
	(4, 'nozevy@mailinator.com', 'nozevy@mailinator.com', '2023-03-23 12:21:42', '2023-03-23 12:21:42', 9),
	(5, 'varokevom@mailinator.com', 'varokevom@mailinator.com', '2023-03-23 12:32:32', '2023-03-23 12:32:32', 10),
	(6, 'nuxejipog@mailinator.com', 'nuxejipog@mailinator.com', '2023-03-23 12:34:20', '2023-03-23 12:34:20', 11),
	(7, 'pypab@mailinator.com', 'pypab@mailinator.com', '2023-03-23 12:38:21', '2023-03-23 12:38:21', 12),
	(8, 'fusem@mailinator.com', 'fusem@mailinator.com', '2023-03-24 00:26:31', '2023-03-24 00:26:31', 13),
	(9, 'Nguyen and Vaughn LLC', 'tixyfygito@mailinator.com', '2023-03-24 02:48:23', '2023-03-24 02:48:23', 13),
	(10, 'Payne and Pruitt Inc', 'gihylyb@mailinator.com', '2023-03-24 02:51:21', '2023-03-24 02:51:21', 13),
	(11, 'Walters Hodge Co', 'xixyxyq@mailinator.com', '2023-03-24 02:54:26', '2023-03-24 02:54:26', 13),
	(12, 'Bean Mcleod Associates', 'geduleqizi@mailinator.com', '2023-03-24 02:55:07', '2023-03-24 02:55:07', 13),
	(13, 'Page Bernard Traders', 'vabyve@mailinator.com', '2023-03-28 14:14:45', '2023-03-28 14:14:45', 13),
	(14, 'The High Table', 'hightable@gmail.com', '2023-03-29 11:24:59', '2023-03-29 11:24:59', 13),
	(15, 'Barron Underwood Trading', 'ziwa@mailinator.com', '2023-03-29 12:32:17', '2023-03-29 12:32:17', 13),
	(16, 'Williams Phillips LLC', 'dafysu@mailinator.com', '2023-03-29 13:22:48', '2023-03-29 13:22:48', 13);

-- Dumping data for table authlock.password_resets: ~0 rows (approximately)

-- Dumping data for table authlock.personal_access_tokens: ~0 rows (approximately)

-- Dumping data for table authlock.types: ~2 rows (approximately)
INSERT INTO `types` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Login', NULL, NULL),
	(2, 'Card', NULL, NULL),
	(3, 'Security note', NULL, NULL);

-- Dumping data for table authlock.users: ~12 rows (approximately)
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'rahat', 'rahat392@gmail.com', NULL, '$2y$10$mS81V1U6zGO6iYcwZBh9lOv2QW8dZnMV8gcL/qF4qzKxGFQ/Zpop2', NULL, '2023-03-20 11:36:52', '2023-03-20 11:36:52'),
	(2, 'Philip Garrison', 'niwelujijo@mailinator.com', NULL, '$2y$10$aO/63qFsMTPLPZfAtUxFr.Cb7IGb2KkI8Tu8c8kcb8AE13Gv1nAWO', NULL, '2023-03-21 08:18:48', '2023-03-21 08:18:48'),
	(3, 'Laith Murphy', 'dotopem@mailinator.com', NULL, '$2y$10$sgLcZh6oB.1JuvlyN6XWd.y8vBRaWTLNc/A9nibrmaWosppXrqoyW', NULL, '2023-03-21 11:36:26', '2023-03-21 11:36:26'),
	(4, 'Morgan Nguyen', 'qapozu@mailinator.com', NULL, '$2y$10$hKwwWAPINHTpQR6jd9o3wuDzyNWmHqWoNOI4972BCdpOgFArhWepe', NULL, '2023-03-22 11:23:42', '2023-03-22 11:23:42'),
	(5, 'Jarrod Romero', 'qyjecyf@mailinator.com', NULL, '$2y$10$HRvaExz1/z7JGw0.cHV.zOhIyNVPokUt9YG.JTRPqZIfrY.QKyTnG', NULL, '2023-03-23 11:32:59', '2023-03-23 11:32:59'),
	(6, 'Avye West', 'zyzycogotu@mailinator.com', NULL, '$2y$10$pm.zPkufjpXUHBCLEpBgWePzj5k.yuAfqjyo3m6Hz7zuIiYR3bFku', NULL, '2023-03-23 12:10:38', '2023-03-23 12:10:38'),
	(7, 'Carol White', 'myjevun@mailinator.com', NULL, '$2y$10$YXfkdzO5N1vBXkAX4eA0zey3HD3HkmJmlDIBvFzYlrv9BaRKBKnN.', NULL, '2023-03-23 12:11:11', '2023-03-23 12:11:11'),
	(8, 'Forrest Hale', 'getul@mailinator.com', NULL, '$2y$10$S7CYDO8zLOtantpghJTLt.uNJeLYCvwNNaUELmWADy//TGBxha3Ay', NULL, '2023-03-23 12:20:35', '2023-03-23 12:20:35'),
	(9, 'Brendan Rodriquez', 'nozevy@mailinator.com', NULL, '$2y$10$V9pMr7See1mlnVKfLQbu/uOkMXT2Us1j2iso9aIsKiQ4TlsZ1Mnsa', NULL, '2023-03-23 12:21:42', '2023-03-23 12:21:42'),
	(10, 'Erasmus Douglas', 'varokevom@mailinator.com', NULL, '$2y$10$0MFr5qERmIC6NTYtJC6r9eeEYS976PssgFhAHkMQGhKZOvFDqDbhy', NULL, '2023-03-23 12:32:32', '2023-03-23 12:32:32'),
	(11, 'Maris Browning', 'nuxejipog@mailinator.com', NULL, '$2y$10$7U7BL4HpEiYjhIXUNh7WEu7qPi/rfhX1MMUuc.rx2l23UdPVamaTG', NULL, '2023-03-23 12:34:20', '2023-03-23 12:34:20'),
	(12, 'Colton Oliver', 'pypab@mailinator.com', NULL, '$2y$10$Dx/uoqNOstfovXuHNj0M7.xfRL.lf5Q8edkITrKr0QL2iKAcZQviu', NULL, '2023-03-23 12:38:21', '2023-03-23 12:38:21'),
	(13, 'Oliver Mcintosh', 'fusem@mailinator.com', NULL, '$2y$10$3y9oNXyXGOk1s/mtOR22IevqH998C4DY7iFoPAfUfSRJHbS1o/BOu', 'a6hQhRpQk8VyMyM9738zQ0heL9lHa7cJyRZTcSWTQG7epRqKOrJBiQ7i5niz', '2023-03-24 00:26:31', '2023-03-24 00:26:31');

-- Dumping data for table authlock.user_organizations: ~16 rows (approximately)
INSERT INTO `user_organizations` (`id`, `user_id`, `organization_id`, `created_at`, `updated_at`) VALUES
	(1, 5, 1, NULL, NULL),
	(2, 7, 2, NULL, NULL),
	(3, 8, 3, NULL, NULL),
	(4, 9, 4, NULL, NULL),
	(5, 10, 5, NULL, NULL),
	(6, 11, 6, NULL, NULL),
	(7, 12, 7, NULL, NULL),
	(8, 13, 8, NULL, NULL),
	(9, 13, 9, NULL, NULL),
	(10, 13, 10, NULL, NULL),
	(11, 13, 11, NULL, NULL),
	(12, 13, 12, NULL, NULL),
	(13, 13, 13, NULL, NULL),
	(14, 13, 14, NULL, NULL),
	(15, 13, 15, NULL, NULL),
	(16, 13, 16, NULL, NULL);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
