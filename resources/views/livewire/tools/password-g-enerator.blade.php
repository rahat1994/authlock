<div class="container main-content">
    <div class="row">
        <ul>
            @if($errors->any())
                {!! implode('', $errors->all('<li style="color:red">:message</li>')) !!}
            @endif
        </ul>
        <div class="col-3">
            <div class="card">
                <div class="card-header">
                    Tools
                </div>

                <div class="card-body">
                    <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <button class="nav-link active" id="v-pills-generator-tab" data-bs-toggle="pill" data-bs-target="#v-pills-generator" type="button" role="tab" aria-controls="v-pills-generator" aria-selected="true">Generator</button>
                        <button class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">Import Data</button>
                        <button class="nav-link" id="v-pills-export-tab" data-bs-toggle="pill" data-bs-target="#v-pills-export" type="button" role="tab" aria-controls="v-pills-export" aria-selected="false">Export Data</button>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-6">
            <div class="col-4">
                <h2 class="h2">{{$heading}}</h2>
            </div>
            <hr>

            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-generator" role="tabpanel" aria-labelledby="v-pills-generator-tab">
                    <livewire:tools.generator-tab>
                </div>

                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">

                    <form method="post" action="{{route('items.import')}}"  enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="form-group">

                                <input type="file" class="form-control" name="import_file" id="file" aria-describedby="fileHelpId" placeholder="">

                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary">Submit</button>

                    </form>


                </div>

                <div class="tab-pane fade" id="v-pills-export" role="tabpanel" aria-labelledby="v-pills-export-tab">


                    <form method="post" action="{{route('items.export')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <div class="form-group">
                                <select class="form-select" aria-label="Default select example" name="format">
                                    <option selected value="csv">CSV</option>
                                    <option value="xlsx">XLSX</option>
                                    <option value="pdf" disabled>PDF</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary">Submit</button>

                    </form>

                </div>
            </div>


        </div>

    </div>
</div>
