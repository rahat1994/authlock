<div style="padding:20px" x-data="generatorData()" x-init="generatePass">


    <div class="row" x-show="passwordType == 'password'">
        <div class="col">
            <div class="p-3 border bg-light" style="text-align:center;" x-text="message"></div>
        </div>
    </div>

    <div class="row" x-show="passwordType == 'passphrase'">
        <div class="col">
            <div class="p-3 border bg-light" style="text-align:center;" x-text="passphrase"></div>
        </div>
    </div>
    <br>

    <h5>What would you like to generate?</h5>
    <div class="row">
        <div class="col">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="password_selector" id="inlineRadio2" value="option2" checked>

                <label class="form-check-label" for="inlineRadio2">Password</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="password_selector" id="inlineRadio1" value="option1" disabled>

                <label class="form-check-label" for="inlineRadio1">UserName</label>
            </div>

        </div>
    </div>
    <br>

    <h5>Password type</h5>
    <div class="row">
        <div class="col">
            <div class="form-check form-check-inline">
                <input x-model="passwordType" class="form-check-input" type="radio" id="inlineRadio1" value="password">
                <label class="form-check-label" for="inlineRadio1">Password</label>
            </div>
            <div class="form-check form-check-inline">
                <input x-model="passwordType" class="form-check-input" type="radio" id="inlineRadio2" value="passphrase">
                <label class="form-check-label" for="inlineRadio2">Passphrase</label>
            </div>
        </div>
    </div>

    <br>

    <span x-show="passwordType == 'password'">
        <h5>Specifications</h5>
        <div class="row">
            <div class="col-md-4">
                <label for="length" class="form-label">Length</label>
                <input x-model="length" type="number" class="form-control" id="length">

            </div>
            <div class="col-md-4">
                <label for="Minimum_numbers" class="form-label">Minimum Numbers</label>
                <input x-model="minimum_numbers" type="number" class="form-control" id="Minimum_numbers">

            </div>
            <div class="col-md-4">
                <label for="Minimum_numbers" class="form-label">Minimum Symbols</label>
                <input x-model="minimum_special" type="number" class="form-control" id="Minimum_numbers">
            </div>
        </div>
    </span>

    <span x-show="passwordType == 'passphrase'">
        <h5>Specifications</h5>
        <div class="row">
            <div class="col-md-4">
                <label for="length" class="form-label">Word count</label>
                <input x-model="passphraseSpecifcations.wordCount" type="number" class="form-control" id="length">

            </div>
            <div class="col-md-4">
                <label for="word_seperator" class="form-label">seperator</label>
                <input x-model="passphraseSpecifcations.seperator" type="text" class="form-control" id="word_seperator">

            </div>
        </div>
    </span>

    <br>
    <span x-show="passwordType == 'password'">
    <h5>Options</h5>
        <div class="row mb-3">
            <div class="col">
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" x-model="options.capitalized_chars">
                    <label class="form-check-label" for="flexSwitchCheckDefault">A-Z</label>
                </div>
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" x-model="options.small_hand_chars">
                    <label class="form-check-label" for="flexSwitchCheckChecked">a-z</label>
                </div>

                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" x-model="options.numbers">
                    <label class="form-check-label" for="flexSwitchCheckChecked">0-9</label>
                </div>

                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" x-model="options.symbols">
                    <label class="form-check-label" for="flexSwitchCheckChecked">!@#$%^&*</label>
                </div>
            </div>
        </div>
    </span>

    

    <br>

    <div class="d-grid gap-2">
        <button x-show="passwordType == 'password'" @click="generatePass" class="btn btn-primary" type="button">Generate Password</button>
        <button x-show="passwordType == 'passphrase'" @click="generatePassphrase" class="btn btn-primary" type="button">Generate Passphrase</button>
        <button class="btn btn-primary" type="button">Copy Password</button>
    </div>
</div>

<script>
    function generatorData() {
        return {
            message: 'I ❤️ Alpine'
            , passphrase: 'password'
            , get copyPass() {
                navigator.clipboard.writeText(pass);
                this.message = 'Password copied to clipboard';
                setTimeout(() => {
                    this.message = 'I ❤️ Alpine';
                }, 2000);
            }
            , passwordType: 'password'
            , length: 8
            , minimum_numbers: 1
            , minimum_special: 1
            , options: {
                capitalized_chars: true
                , small_hand_chars: true
                , numbers: true
                , symbols: false
            , },
            passphraseSpecifcations: {
                wordCount: 4
                , seperator: '-'}
        , }
    }

    function setMessage() {
        this.message = 'hello there';
    }

    function generatePassphrase() {
        <?php
            // dd($this->passphraseWordsArray);    
        ?>
        // Define word list for passphrase generation
        const wordList = @js(json_decode($passphraseWordsArray));

        console.log(wordList);
        
        let passphrase = '';
        
        // Generate passphrase by selecting random words from the word list
        for (let i = 0; i < this.passphraseSpecifcations.wordCount; i++) {
            const index = Math.floor(Math.random() * wordList.length);
            passphrase += wordList[index];
            
            if (i !== this.passphraseSpecifcations.wordCount - 1) {
                passphrase += this.passphraseSpecifcations.seperator;
            }
        }
    
        this.passphrase =  passphrase;
    }

    function generatePass() {

        if(this.passwordType === 'passphrase'){
            this.passphrase = generatePassphrase();
        }
        let charSet = '';
        let password = '';

        if (this.options.capitalized_chars) {
            charSet += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        if (this.options.small_hand_chars) {

            charSet += 'abcdefghijklmnopqrstuvwxyz';
        }

        if (this.options.numbers) {

            charSet += '0123456789';
        }

        if (this.options.symbols) {

            charSet += '!@#$%^&*()_+-=';
        }

        if (charSet === '') {
            this.options.capitalized_chars = true;
            charSet += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }


        const numCount = this.minimum_numbers;
        const symCount = this.minimum_special;
        const length = this.length;

        if (!this.options.capitalized_chars && !this.options.small_hand_chars && !this.options.numbers && !this.options.symbols) {
            this.options.capitalized_chars = true;
        }

        // Determine the maximum number of numbers and symbols to add to the password
        const maxNumCount = Math.min(numCount, length - 1);
        const maxSymCount = Math.min(symCount, length - maxNumCount - 1);

        this.numCount = maxNumCount;
        this.symCount = maxSymCount;

        // Add characters to password
        for (let i = 0; i < length - maxNumCount - maxSymCount; i++) {
            password += charSet.charAt(Math.floor(Math.random() * charSet.length));
        }
        // Add numbers to password
        for (let i = 0; i < maxNumCount; i++) {
            password += '0123456789'.charAt(Math.floor(Math.random() * 10));
        }

        // Add symbols to password
        for (let i = 0; i < maxSymCount; i++) {
            password += '!@#$%^&*()_+-='.charAt(Math.floor(Math.random() * 16));
        }

        // Shuffle the password characters
        password = password.split('').sort(() => Math.random() - 0.5).join('');

        this.message = password;
    }

</script>
