<div class="row" style="padding: 0px 15px" 
    x-data="itemsTableComponentData()" 
    x-on:item-checkbox-clicked="itemSelected($event.detail)">


    <span wire:loading>
        <div class="d-flex justify-content-center">
            <div class="spinner-border" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
    </span>

    @if (count($items) == 0)
    <div class="alert alert-info" role="alert">
        No items found.
    </div>
    @else

    <div x-show="selectedItems.length > 0" class="btn-group" role="group" id="mass-action-group" wire:loading.remove x-transition x-cloak>

        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#folderSelectingModal">Move selected items to a new folder</button>
        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteSelectedItemsModal">Delete selected items</button>

    </div>

    <div class="input-group mb-3" style="margin: 15px 0px;" wire:loading.remove>

        <input @keyup="rowShouldBeVisible()" type="text" class="form-control" placeholder="Search" x-model="searchTerm">
        @error('folder_name') <span class="error">{{ $message }}</span> @enderror
    </div>


    <!-- Selected Items folder changing modal -->
    <div class="modal fade" id="folderSelectingModal" tabindex="-1" aria-labelledby="folderSelectingModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" x-text="moveModalTitle()"></h5>

                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <select class="form-select" aria-label="Default select example" x-model="folderForMovingItems">
                        @foreach($folders as $key => $folder)
                        <option value="{{$folder->id}}" >{{$folder->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button @click="$wire.moveItemsToFolder()" type="button" class="btn btn-primary">Move</button>

                </div>
            </div>
        </div>
    </div>


    <!-- Selected Items delete modal -->
    <div class="modal fade" id="deleteSelectedItemsModal" tabindex="-1" aria-labelledby="deleteSelectedItemsModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Multiple Items</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h3>Are you sure you want to delete these Items?</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" @click="$wire.deleteSelectedItems()">Confirm</button>

                </div>
            </div>
        </div>
    </div>


    <table class="table table-hover" wire:loading.remove>


        <thead>
            <tr>
                <th>
                    <input @click="selectAllItems()" class="form-check-input mt-0 select-all-items" type="checkbox" value="" aria-label="Checkbox for following text input"> All
                </th>
                <th>Name</th>
                <th>Owner</th>
                <th>
                    <span class="px-2" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                    </span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($items as $index => $item)

            @if($item->in_trash == 1 ||
            ($selected_folder != null && $selected_folder != 'no_folder' && $item->folder_id != $selected_folder) ||
            ($selected_organization != null && $item->organization_id != $selected_organization) ||
            ($selected_type != null && $item->type_id != $selected_type)
            )
            @continue
            @endif

            @if (($selected_folder == 'no_folder' && $item->folder_id != null))
            @continue
            @endif

            <tr class="item-table-row" data-name="{{$item->name}}" wire:key="item-row-{{$item->id}}" x-data="{
                            item_id: {{$item->id}},
                            item_username: '{{$item->username}}',
                            item_password: '{{$item->password}}',
                            item_name: '{{$item->name}}'
                        }">
                <td>
                    <input class="form-check-input mt-0 select-item" data-itemIndex="{{$item->id}}" x-on:click="$dispatch('item-checkbox-clicked', {item_id: item_id, item_username: item_username, item_password: item_password, item_name: item_name})" type="checkbox" value="" aria-label="Checkbox for following text input">
                </td>
                <td>
                    <div class="ms-2 me-auto">
                        <div class="fw-bold">
                            <a role="button" class="link-primary" wire:click="itemSelectedToUpdate({{$item->id}})">{{$item->name}}</a>

                        </div>
                        {{$item->username}}

                        <a role="button" class="link-primary" x-on:click="copyPass('{{$item->password}}')">
                            <i class="fa fa-copy" aria-hidden="true"></i>
                        </a>
                    </div>
                </td>
                <td>
                    <span class="badge rounded-pill bg-success">{{$item->organization->name}}</span>
                </td>
                <td>

                    <div class="btn-group">


                        <span class="px-2" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                        </span>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" role="button" class="link-primary" x-on:click="copyPass('{{$item->username}}')"><i class="fa fa-copy" aria-hidden="true"></i> Copy Username</a></li>
                            <li><a class="dropdown-item" role="button" class="link-primary" x-on:click="copyPass('{{$item->password}}')"><i class="fa fa-copy" aria-hidden="true"></i> Copy Password</a></li>
                            <li><a class="dropdown-item" role="button" wire:click="deleteItem({{$item->id}})"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>


            @endforeach
        </tbody>
    </table>
    @endif


    <script>
        function closeAllModals(){
            var itemUpdateModals = document.getElementsByClassName('modal');

            for (var i = 0; i < itemUpdateModals.length; i++) {
                //itemUpdateModals[i].style.visibility = "hidden"; // or
                // itemUpdateModals[i].style.display = "none"; // depending on what you're doing
                console.log(itemUpdateModals);
                var modal = bootstrap.Modal.getInstance(itemUpdateModals[i]);

                if (modal != null) {
                    modal.hide();
                }

            }
        }

        window.addEventListener('itemUpdated', event => {

            closeAllModals();
            swal({
                title: "Item Updated!"
                , text: "Item has been Updated successfully!"
                , position: 'top-end'
                , icon: "success"
                , timer: 1000
            , });
        });

        window.addEventListener('itemUpdateFailed', event => {

            swal({
                position: 'top-end'
                , title: "Item update Failed!"
                , text: "Something went wrong!"
                , icon: "error"
                , timer: 1000
            , });
        });

        window.addEventListener('itemsTable.selectedItemsMoved', event => {
            closeAllModals();
            var items = document.getElementsByClassName('item-table-row');
            
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var checkbox = item.getElementsByClassName('select-item')[0];
                checkbox.checked = false;
            }

            document.getElementById('mass-action-group').style.display = "none";
            swal({
                title: "Items Moved!"
                , text: "Items has been moved successfully!"
                , position: 'top-end'
                , icon: "success"
                , timer: 1000
            , });
        });

        window.addEventListener('itemsTable.selectedItemsDeleted', event => {
            closeAllModals();
            var items = document.getElementsByClassName('item-table-row');
            
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var checkbox = item.getElementsByClassName('select-item')[0];
                checkbox.checked = false;
            }
            document.getElementById('mass-action-group').style.display = "none";
            swal({
                title: "Items Deleted!"
                , text: "Items deleted successfully!"
                , position: 'top-end'
                , icon: "success"
                , timer: 1000
            , });
        });

        window.addEventListener('itemsTable.somethingwentwrong', event => {
            closeAllModals();
            var items = document.getElementsByClassName('item-table-row');
            
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var checkbox = item.getElementsByClassName('select-item')[0];
                checkbox.checked = false;
            }
            document.getElementById('mass-action-group').style.display = "none";
            swal({
                title: "Something went wrong!"
                , text: "something went wrong."
                , position: 'top-end'
                , icon: "error"
                , timer: 1000
            , });
        });

        function itemsTableComponentData() {
            return {
                searchTerm: ''
                , moveModalTitle() {
                    return `Move ${this.selectedItems.length} items to another folder`
                }
                , selectedItems: @entangle('selectedItems').defer
                , folderForMovingItems: @entangle('folder_for_moving_items').defer
                , rowShouldBeVisible() {
                    var items = document.getElementsByClassName('item-table-row');
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        var name = item.getAttribute('data-name');
                        if (name.toLowerCase().includes(this.searchTerm.toLowerCase())) {
                            item.style.display = "";
                        } else {
                            item.style.display = "none";
                        }
                    }
                }
                , itemSelected(eventDetails) {
                    if (this.selectedItems.includes(eventDetails.item_id)) {
                        this.selectedItems = this.selectedItems.filter(function(value, index, arr) {
                            return value != eventDetails.item_id;
                        });
                    } else {
                        this.selectedItems.push(eventDetails.item_id);
                    }
                }
                , moveItemsToFolder() {
                    // var folderId = document.getElementById('move-items-to-folder').value;
                    console.log(this.selectedItems);
                    // @this.moveItemsToFolder(folderId, this.selectedItems);
                }
                , deketeSelectedItems() {
                    console.log(this.selectedItems);
                    @this.deleteItems(this.selectedItems);
                }
                , selectAllItems() {
                    var items = document.getElementsByClassName('item-table-row');
                    var selectAllItems = document.getElementsByClassName('select-all-items');
                    var selectAllItemsChecked = selectAllItems[0].checked;

                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        var checkbox = item.getElementsByClassName('select-item')[0];
                        console.log(checkbox);
                        checkbox.checked = selectAllItemsChecked;
                        if (selectAllItemsChecked) {
                            this.selectedItems.push(checkbox.getAttribute('data-itemIndex'));
                        } else {
                            this.selectedItems = [];
                        }
                    }
                }
            , }
        }

    </script>
</div>
