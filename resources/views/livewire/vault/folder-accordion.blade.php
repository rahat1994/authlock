<div class="m-2 accordion-item" x-data="folderAccordionData()">

    <h2 class="accordion-header" id="panelsStayOpen-headingfour">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapsefour" aria-expanded="true" aria-controls="panelsStayOpen-collapsefour">
            Folders
        </button>
    </h2>
    <div id="panelsStayOpen-collapsefour" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingfour">
        <div class="accordion-body">
            <ul class="nav flex-column">
                @if (count($folders) > 0)
                @foreach ($folders as $folder)
                <li class="nav-item editable-nav-item">
                    <a class="nav-link" aria-current="page" role="button" wire:click="selectFolder({{$folder}})"> <i class="fa fa-folder"> </i> {{ $folder->name }}</a>


                    <a @click="showFolderEditModeal('{{ $folder->name }}', '{{ $folder->id }}')" class="nav-link" aria-current="page" role="button" data-bs-toggle="modal" data-bs-target="#folderEditingModal">
                        <i class="fa fa-edit"> </i>
                    </a>
                </li>
                @endforeach
                @endif

                <li class="nav-item editable-nav-item">
                    <a class="nav-link" aria-current="page" role="button" wire:click="selectFolder()"> <i class="fa fa-folder"> </i> No Folder</a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" aria-current="page" role="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop"><i class="fa fa-plus"> </i>
                        New Folder
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- Edit Modal -->
    <div wire:ignore.self class="modal fade" id="folderEditingModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="folderEditingModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">New Folder</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form wire:submit.prevent="editFolder">

                        <div class="input-group">
                            <input type="hidden" x-model="editfolderId">
                            <input type="text" x-model="editFolderName" class="form-control" placeholder="Folder name">
                            <button type="submit" class="btn btn-primary" type="button">Save</button>
                        </div>
                        @error('folder_name') <span class="error invalid-feedback">{{ $message }}</span> @enderror



                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                    <button @click="$wire.deleteFolder()" type="button" class="btn btn-danger">
                        <i class="fa fa-trash"> </i>
                    </button>
                    <!-- <button type="button" class="btn btn-primary">Understood</button> -->
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">New Folder</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form wire:submit.prevent="createFolder">
                        <div class="input-group">
                            <input type="text" wire:model.defer="folder_name" class="form-control" placeholder="Folder name">
                            <button type="submit" class="btn btn-primary" type="button">Save</button>
                        </div>
                        @error('folder_name') <span class="error">{{ $message }}</span> @enderror
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Understood</button> -->
                </div>
            </div>
        </div>
    </div>

    <script>
        window.addEventListener('folderAccordion.folderUpdate', event => {
            if (event.detail.eventLabel === 'Folder Created') {
                var folderModal = document.getElementById('staticBackdrop');
            } else {
                var folderModal = document.getElementById('folderEditingModal');
            }

            swal({
                title: event.detail.eventLabel
                , text: event.detail.message
                , position: 'top-end'
                , icon: "success"
                , timer: 3000
            , });

            var modal = bootstrap.Modal.getInstance(folderModal);
            modal.hide();
        });

        function folderAccordionData() {
            return {
                editFolderName: @entangle('edit_folder_name').defer
                , editfolderId: @entangle('edit_folder_id').defer
                , showFolderEditModeal(folderName, folderId) {
                    this.editFolderName = folderName;
                    this.editfolderId = folderId;
                }
                , updateFolder() {
                    $wire.editFolder();
                }
            }
        }

    </script>
</div>
