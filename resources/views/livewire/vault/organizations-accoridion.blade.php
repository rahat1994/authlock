<div class="m-2 accordion-item">

    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
            All Vaults
        </button>
    </h2>
    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
        <div class="accordion-body">
            <ul class="nav flex-column">

                @if (count($organizations) > 0)
                @foreach ($organizations as $organization)
                <li class="nav-item">
                    <a class="nav-link" role="button" aria-current="page" wire:click="selectOrganization({{$organization}})"> <i class="fa fa-building"> </i> {{$organization->name}}</a>
                </li>
                @endforeach
                @endif
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" role="button" data-bs-toggle="modal" data-bs-target="#organization_creation_modal"> <i class="fa fa-plus"> </i> New Organization</a>
                </li>
            </ul>
        </div>
    </div>

    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="organization_creation_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">New Organization</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form wire:submit.prevent="createOrganization">
                        <div class="mb-3">
                            <label for="organization_name" class="form-label">Organization Name</label>
                            <input wire:model.defer="organizationName" type="text" class="form-control" id="organization_name">
                        </div>
                        @error('organizationName') <span class="error" class="invalid-feedback">{{ $message }}</span> @enderror



                        <div class="mb-3">
                            <label for="billing_email" class="form-label">Billing Email</label>
                            <input wire:model="billingAddress" type="email" class="form-control" id="billing_email">
                        </div>

                        @error('billingAddress') <span class="error">{{ $message }}</span> @enderror



                        <!-- <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div> -->
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary" type="submit"> Submit </button>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Understood</button> -->
                </div>
            </div>
        </div>
    </div>

    <script>
        window.addEventListener('organizationCreated', event => {

            var folderModal = document.getElementById('organization_creation_modal');
            var modal = bootstrap.Modal.getInstance(folderModal);
            modal.hide();
            swal({
                title: "Organization Created!"
                , text: "Organization has been created successfully!"
                , position: 'top-end'
                , icon: "success"
                , timer: 1000
            , });
        });

        window.addEventListener('organizationCreationFailed', event => {

            swal({
                position: 'top-end'
                , title: "Organization Creataion Failed!"
                , text: "Something went wrong!"
                , icon: "error"
                , timer: 1000
            , });
        });

    </script>
</div>
