<div class="m-2 accordion-item">

    <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">
            All Items
        </button>
    </h2>
    <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
        <div class="accordion-body">
            <ul class="nav flex-column">
                @if (count($types) > 0)
                @foreach ($types as $type)
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" role="button" wire:click="selectItemType({{$type}})"> <i class="fa fa-address-card"> </i> {{$type->name}}</a>
                </li>
                @endforeach

                @endif
            </ul>
        </div>
    </div>
</div>
