<div class="row justify-content-between">
    <div class="col-4">
        <h2 class="h2">{{$heading}}</h2>
    </div>

    <div class="col-3 ml-auto">
        <a class="btn btn-primary new_item_button" href="#" role="button" data-bs-toggle="modal" data-bs-target="#item_creation_modal"><i class="fa fa-plus" aria-hidden="true"></i> New Item</a>
    </div>

    <!-- Item creation Modal -->
    <div wire:ignore.self class="modal fade" id="item_creation_modal" tabindex="-1" aria-labelledby="item_creation_modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="item_creation_modalLabel">Add Item</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="row g-3" wire:submit.prevent="createItem">
                        <div class="col-md-6">
                            <label for="item_type" class="form-label">What type of the item is this?</label>
                            <select wire:model="itemType" class="form-control" name="item_type" id="item_type" name="item_type">
                                @if(count($itemTypes) > 0)
                                @foreach($itemTypes as $key => $value)
                                <option value="{{$value->id}}" @if($key==0) selected @endif>{{$value->name}}</option>
                                @endforeach

                                @endif
                                <select>
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-6">
                            <label for="item_name" class="form-label">Name</label>
                            <input wire:model.defer="itemName" type="text" class="form-control @error('itemName') is-invalid  @enderror" id="item_name" placeholder="">
                            @error('itemName')
                            <div id="item_name" class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label for="item_folder" class="form-label">Folder</label>
                            <select wire:model.defer="itemFolder" class="form-control" id="item_folder" name="item_folder">

                                @if (count($folders) > 0)
                                @foreach ($folders as $folder)
                                <option value="{{ $folder->id }}">{{ $folder->name }}</option>
                                @endforeach
                                @endif
                                <select>
                        </div>

                        <div class="col-6">
                            <label for="item_username" class="form-label">Username</label>
                            <input wire:model.defer="itemUserName" type="text" class="form-control @error('itemUserName') is-invalid  @enderror" id="item_username" name="username" placeholder="">

                            @error('itemUserName')

                            <div id="item_username" class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror

                        </div>

                        <div class="col-6" x-data="{ showPassword: false }">
                            <label for="item_password" class="form-label">Password</label>
                            <div class="input-group">
                                <input class="form-control @error('itemPassword') is-invalid @enderror" x-ref="password_field" wire:model.defer="itemPassword" x-bind:type="showPassword ? 'text' : 'password'" id="item_password" placeholder="">
                                <button x-on:click="$wire.itemPassword = randomPassword()" class="btn btn-outline-secondary" type="button"><i class="fa fa-refresh"></i></button>

                                <button x-on:click="showPassword = ! showPassword" class="btn btn-outline-secondary" type="button"><i class="fa fa-eye"></i></button>
                                <button x-on:click="copyPass($refs.password_field.value)" class="btn btn-outline-secondary" type="button"><i class="fa fa-copy"></i></button>
                                @error('itemPassword')

                                <div id="item_password" class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror

                            </div>



                        </div>

                        <div class="col-12">
                            <label for="item_login_url" class="form-label">login Url</label>
                            <input wire:model.defer="loginUrl" type="text" class="form-control @error('loginUrl') is-invalid @enderror" id="item_login_url" placeholder="i.e:  gmail.com">


                            @error('loginUrl')
                            <div id="item_login_url" class="invalid-feedback">

                                {{ $message }}
                            </div>
                            @enderror

                        </div>




                        <div class="col-md-12">
                            <label for="item_note" class="form-label">Note</label>
                            <textarea wire:model.defer="itemNote" class="form-control" id="item_note">

                        </textarea>
                        </div>

                        <div class="col-12">
                            <!-- <label organization" class="form-label">Ownership</label> -->
                            <label for="item_organization" class="form-label">Who owns this item?</label>
                            <select wire:model.defer="itemOrganization" class="form-control" id="item_organization" name="item_organization">

                                @if (count($organizations) > 0)
                                @foreach ($organizations as $organization)
                                <option value="{{ $organization->id }}">{{ $organization->name }}</option>
                                @endforeach
                                @endif
                                <select>
                        </div>
                        <br>
                        <div class="col-12">
                            <div class="form-check">
                                <input wire:model.defer="itemMasterPassSecured" class="form-check-input" type="checkbox" value="" id="secureByMasterPassword">
                                <label class="form-check-label" for="secureByMasterPassword">
                                    Secure By master password
                                </label>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-check">
                                <input wire:model.defer="itemIsFavourite" class="form-check-input" type="checkbox" value="" id="is_favourite">
                                <label class="form-check-label" for="is_favourite">
                                    Is Favourite
                                </label>
                            </div>
                        </div>



                        <br>
                        <br>
                        <button class="btn btn-primary" type="submit"> Create Item</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>

    <script>
        window.addEventListener('itemCreated', event => {

            var folderModal = document.getElementById('item_creation_modal');
            var modal = bootstrap.Modal.getInstance(folderModal);

            console.log(modal)
            modal.hide();
            swal({
                title: "Item Created!"
                , text: "Item has been created successfully!"
                , position: 'top-end'
                , icon: "success"
                , timer: 1500
            , });
        });

        window.addEventListener('itemCreationFailed', event => {

            swal({
                position: 'top-end'
                , title: "Organization Creataion Failed!"
                , text: "Something went wrong!"
                , icon: "error"
                , timer: 1500
            , });
        });

        function randomPassword() {
            var randomstring = Math.random().toString(36).slice(-8);
            return randomstring;
        }

        function copyPass(password) {
            navigator.clipboard.writeText(password);
        }

    </script>
</div>
