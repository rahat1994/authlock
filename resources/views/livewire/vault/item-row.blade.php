<tr>
    <td >{{$index + 1}}</td>
    <td>
    <div class="ms-2 me-auto">
        <div class="fw-bold">
            <a class="link-primary" data-bs-toggle="modal" data-bs-target="#item_update_modal_{{$index}}">{{$item->name}}</a>
        </div>
        {{$item->username}}

        <a href="#">
            <i class="fa fa-copy" aria-hidden="true" ></i>
        </a>
    </div>
    </td>
    <td >
        <span class="badge rounded-pill bg-success">{{$item->organization->name}}</span>
    </td>
    <td >

        <div class="btn-group">


            <span class="px-2" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-ellipsis-v" aria-hidden="true" ></i>
            </span>
            <!-- <button type="button" class="btn btn-danger dropdown-toggle" >

            </button> -->
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="#"><i class="fa fa-copy" aria-hidden="true" ></i>  Copy Username</a></li>
                <li><a class="dropdown-item" href="#"><i class="fa fa-copy" aria-hidden="true" ></i> Copy Password</a></li>
                <li><a class="dropdown-item" href="#"><i class="fa fa-trash" aria-hidden="true" ></i> Delete</a></li>
            </ul>
        </div>
    </td>

    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="item_update_modal_{{$index}}" tabindex="-1" aria-labelledby="item_update_modal_{{$index}}Label" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="item_update_modal_{{$index}}Label">Add Item</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form class="row g-3" wire:submit.prevent="updateItem">
                    <div class="col-md-6">
                        <label for="item_type_{{$index}}" class="form-label">What type of the item is this?</label>
                        <select wire:model="itemType" class="form-control" name="item_type" id="item_type_{{$index}}" name="item_type_{{$index}}">
                            @if(count($itemTypes) > 0)
                                @foreach($itemTypes as $key => $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                @endforeach

                            @endif
                        <select>
                    </div>
                    <div class="col-md-6">
                    </div>
                    <div class="col-6">
                        <label for="item_name_{{$index}}" class="form-label">Name</label>
                        <input wire:model.defer="itemName" type="text" class="form-control" id="item_name_{{$index}}" placeholder="">
                    </div>
                    <div class="col-6">
                        <label for="item_folder_{{$index}}" class="form-label">Folder</label>
                        <select wire:model.defer="itemFolder" class="form-control" id="item_folder_{{$index}}" name="item_folder_{{$index}}">

                            @if (count($folders) > 0)
                                @foreach ($folders as $folder)
                                    <option value="{{ $folder->id }}">{{ $folder->name }}</option>
                                @endforeach
                            @endif
                        <select>
                    </div>

                    <div class="col-6">
                        <label for="item_username_{{$index}}" class="form-label">Username</label>
                        <input wire:model.defer="itemUserName" type="text" class="form-control" id="item_username_{{$index}}" name="username" placeholder="">
                    </div>

                    <div class="col-6" x-data="{ showPassword: false }">
                        <label for="item_password_{{$index}}" class="form-label">Password</label>
                        <div class="input-group" >
                            <input x-ref="password_field" wire:model.defer="itemPassword" x-bind:type="showPassword ? 'text' : 'password'" class="form-control" id="item_password_{{$index}}" name="password" placeholder="">

                            <button x-on:click="$refs.password_field.value = randomPassword()" class="btn btn-outline-secondary" type="button"><i class="fa fa-refresh"></i></button>
                            <button x-on:click="showPassword = ! showPassword" class="btn btn-outline-secondary" type="button"><i class="fa fa-eye"></i></button>
                            <button x-on:click="copyPass($refs.password_field.value)" class="btn btn-outline-secondary" type="button"><i class="fa fa-copy"></i></button>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label for="item_note_{{$index}}" class="form-label">Note</label>
                        <textarea wire:model.defer="itemNote" class="form-control" id="item_note_{{$index}}">

                        </textarea>
                    </div>

                    <div class="col-12">
                        <!-- <label for="item_folder" class="form-label">Ownership</label> -->
                        <label for="item_organization_{{$index}}" class="form-label">Who owns this item?</label>
                        <select wire:model.defer="itemOrganization" class="form-control" id="item_organization_{{$index}}" name="item_organization_{{$index}}">

                                @if (count($organizations) > 0)
                                    @foreach ($organizations as $organization)
                                        <option value="{{ $organization->id }}">{{ $organization->name }}</option>
                                    @endforeach
                                @endif
                        <select>
                    </div>
                    <br>
                    <div class="col-12">
                        <div class="form-check">
                            <input wire:model.defer="itemMasterPassSecured" class="form-check-input" type="checkbox" value="" id="secureByMasterPassword_{{$index}}">
                            <label class="form-check-label" for="secureByMasterPassword_{{$index}}">
                                Secure By master password
                            </label>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-check">
                            <input wire:model.defer="itemIsFavourite" class="form-check-input" type="checkbox" value="" id="is_favourite_{{$index}}">
                            <label class="form-check-label" for="is_favourite_{{$index}}">
                                Is Favourite
                            </label>
                        </div>
                    </div>



                    <br>
                    <br>
                    <button class="btn btn-primary" type="submit"> Update Item</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
            </div>
        </div>
    </div>
</tr>
