<div class="container main-content" x-data="vaultViewData()">
    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-header">
                    Filters
                </div>
                <div class="card-body" style="padding:0;">​​

                    <div class="accordion vault-accoridions" id="accordionPanelsStayOpenExample">
                        <span x-show="!viewTrash" x-transition>
                            <livewire:vault.query-list-component :query="$query" />
                            <livewire:vault.organizations-accoridion :user="$user" :organizations="$organizations" />
                            <livewire:vault.items-accordion />
                            <livewire:vault.folder-accordion :folders="$folders" :user="$user" />


                            <div class="m-2 accordion-item">

                                <h2 class="accordion-header" id="panelsStayOpen-headingThree">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="true" aria-controls="panelsStayOpen-collapseThree">
                                        Collections
                                    </button>
                                </h2>
                                <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingThree">
                                    <div class="accordion-body">
                                        <ul class="nav flex-column">
                                            {{-- <li class="nav-item">
                                                <a class="nav-link active" aria-current="page" href="#"> <i class="fa fa-home"> </i> Favourites</a>
                                            </li> --}}
                                            {{-- <li class="nav-item">
                                                <a class="nav-link" href="google.com"> <i class="fa fa-home"> </i> Link</a>
                                            </li> --}}
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </span>


                        <div class="m-2 d-grid gap-2">
                            <a href="{{route('tools')}}" class="btn btn-primary" type="button"><i class="fa fa-cog"> </i> {{ __('Tools') }}</a>
                        </div>

                        <span x-show="!viewTrash" x-transition>

                            <div class="m-2 d-grid gap-2">
                                <button @click="viewTrash = true" class="btn btn-outline-primary" type="button"><i class="fa fa-trash"> </i> {{ __('View Trash') }}</button>

                            </div>

                        </span>


                        <span x-show="viewTrash" x-transition>


                            <div class="m-2 d-grid gap-2">
                                <button @click="viewTrash = false" class="btn btn-outline-primary" type="button"><i class="fa fa-list"> </i> {{ __('View Items') }}</button>

                            </div>

                        </span>




                    </div>

                </div>


            </div>
        </div>
        ​
        <div class="col-9" x-show="!viewTrash && !itemEditMode" x-transition>
            <livewire:vault.main-content-header :heading="$heading" :folders="$user->folders" :user="$user" />
            <livewire:vault.items-table :query="$query" :items="$items" :folders="$folders" :organizations="$organizations" />
        </div>


        <div class="col-9" x-show="itemEditMode" x-transition.duration.800ms>
            <livewire:vault.item-edit-view :item="$itemToEdit" :folders="$folders" :organizations="$organizations" :types="$types" />

        </div>


        <div class="col-9" x-show="viewTrash" x-transition>

            {{-- <livewire:vault.main-content-header :heading="$heading" :folders="$user->folders" :user="$user" /> --}}
            <livewire:vault.trashed-items :items="$items" :folders="$folders" :organizations="$organizations" />

        </div>

    </div>


</div>

<script>
    function vaultViewData() {
        return {
            viewTrash: false
            , itemEditMode: @entangle('itemEditMode')
            , items: @entangle('items')
            , folders: @entangle('folders')
            , organizations: @entangle('organizations')
            , heading: @entangle('heading')
            , user: @entangle('user')
            , changeContent() {
                this.heading = 'test';
            }
        }
    }

</script>
