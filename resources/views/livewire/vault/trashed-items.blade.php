<div class="row" style="padding: 0px 15px ">
    <table class="table table-hover">
        <thead>
            <tr>
                <th># All</th>
                <th>Name</th>
                <th>Owner</th>
                <th>
                    <span class="px-2" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                    </span>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($trashedItems as $index => $item)

            <tr wire:key="item-row-{{$item->id}}" x-data="{
                        item_id: {{$item->id}},
                        item_username: '{{$item->username}}',
                        item_password: '{{$item->password}}',
                    }">
                <td>{{$index + 1}}</td>
                <td>
                    <div class="ms-2 me-auto">
                        <div class="fw-bold">
                            <a role="button" class="link-primary" data-bs-toggle="modal" data-bs-target="#item_update_modal_{{$index}}">{{$item->name}}</a>
                        </div>
                        {{$item->username}}

                        <a role="button" class="link-primary" x-on:click="copyPass('{{$item->password}}')">
                            <i class="fa fa-copy" aria-hidden="true"></i>
                        </a>
                    </div>
                </td>
                <td>
                    <span class="badge rounded-pill bg-success">{{$item->organization->name}}</span>
                </td>
                <td>

                    <div class="btn-group">


                        <span class="px-2" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                        </span>
                        <!-- <button type="button" class="btn btn-danger dropdown-toggle" >

                            </button> -->
                        <ul class="dropdown-menu">
                            {{-- <li><a class="dropdown-item" role="button" class="link-primary" x-on:click="copyPass('{{$item->username}}')"><i class="fa fa-copy" aria-hidden="true"></i> Copy Username</a></li>
                            <li><a class="dropdown-item" role="button" class="link-primary" x-on:click="copyPass('{{$item->password}}')"><i class="fa fa-copy" aria-hidden="true"></i> Copy Password</a></li> --}}
                            <li><a class="dropdown-item" role="button" wire:click="resotreItem({{$item->id}})"><i class="fa fa-refresh" aria-hidden="true"></i> Restore</a></li>
                            <li><a class="dropdown-item" role="button" wire:click="deletePermamnently({{$item->id}})"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a></li>





                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <script>
        window.addEventListener('itemUpdated', event => {

            var itemUpdateModals = document.getElementsByClassName('modal');

            for (var i = 0; i < itemUpdateModals.length; i++) {
                //itemUpdateModals[i].style.visibility = "hidden"; // or
                // itemUpdateModals[i].style.display = "none"; // depending on what you're doing
                console.log(itemUpdateModals);
                var modal = bootstrap.Modal.getInstance(itemUpdateModals[i]);

                if (modal != null) {
                    modal.hide();
                }

            }
            swal({
                title: "Organization Created!"
                , text: "Organization has been created successfully!"
                , position: 'top-end'
                , icon: "success"
                , timer: 1000
            , });
        });

        window.addEventListener('itemUpdateFailed', event => {

            swal({
                position: 'top-end'
                , title: "Organization Creataion Failed!"
                , text: "Something went wrong!"
                , icon: "error"
                , timer: 1000
            , });
        });

    </script>
</div>
