<div>
    <form class="row g-3" wire:submit.prevent="updateItemGoBackToList">



        <div class="col-md-6">
            <label for="item_type" class="form-label">What type of the item is this?</label>
            <select wire:model="itemType" class="form-control" name="item_type" id="item_type_update">
                @if(count($types) > 0)
                @foreach($types as $key => $value)

                <option value="{{$value->id}}" @if($value->id == $itemType)
                    selected="selected" @endif>{{$value->name}}</option>
                @endforeach

                @endif
                <select>
        </div>


        <div class="col-6">
            <label for="item_name_update" class="form-label">Name</label>
            <input wire:model.defer="itemName" type="text" class="form-control @error('itemName') is-invalid @enderror" id="item_name_update" placeholder="">

            @error('itemName')
            <div id="item_name_error" class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror

        </div>


        <div class="col-6">
            <label for="item_folder_update" class="form-label">Folder</label>
            <select wire:model.defer="itemFolder" class="form-control" id="item_folder_update">
                @if (count($folders) > 0)
                @foreach ($folders as $folder)
                <option value="{{ $folder->id }}" @if($folder->id == $itemFolder)
                    selected="selected" @endif>{{ $folder->name }}</option>
                @endforeach
                @endif
                <select>
        </div>

        <div class="col-6">
            <label for="item_username_update" class="form-label">Username</label>
            <input wire:model.defer="itemUserName" type="text" class="form-control @error('itemUserName') is-invalid @enderror" id="item_username_update" placeholder="">


            @error('itemUserName')
            <div id="item_username_update_error" class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror

        </div>

        <div class="col-6">
            <label for="item_username_update" class="form-label">Login URL</label>
            <input wire:model.defer="itemLoginUrl" type="text" class="form-control @error('itemLoginUrl') is-invalid @enderror" id="item_login_url_update" placeholder="">


            @error('itemLoginUrl')
            <div id="item_username_update_error" class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror

        </div>

        <div class="col-6" x-data="{ showPassword: false }">
            <label for="item_password_update" class="form-label">Password</label>
            <div class="input-group">
                <input x-ref="password_field" wire:model.defer="itemPassword" x-bind:type="showPassword ? 'text' : 'password'" class="form-control">

                {{-- <button x-on:click="document.getElementById('item_password_{{$index}}').value = randomPassword()" class="btn btn-outline-secondary" type="button"><i class="fa fa-refresh"></i></button> --}}
                <button x-on:click="showPassword = ! showPassword" class="btn btn-outline-secondary" type="button"><i class="fa fa-eye"></i></button>
                <button x-on:click="copyPass($refs.password_field.value)" class="btn btn-outline-secondary" type="button"><i class="fa fa-copy"></i></button>

                @error('itemPassword')
                <div id="item_password_update" class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror

            </div>
        </div>

        <div class="col-md-12">
            <label for="item_note_update" class=" form-label">Note</label>
            <textarea wire:model="itemNote" class="form-control" id="item_note_update">

            </textarea>
        </div>

        <div class="col-12">
            <!-- <label for="item_folder" class="form-label">Ownership</label> -->
            <label for="item_organization_update" class="form-label">Who owns this item?</label>
            <select wire:model.defer="itemOrganization" class="form-control" id="item_organization_update">

                @if (count($organizations) > 0)
                @foreach ($organizations as $organization)
                <option value="{{ $organization->id }}" @if($organization->id == $itemOrganization)

                    selected="selected" @endif>{{ $organization->name }}</option>
                @endforeach
                @endif
                <select>
        </div>
        <br>
        <div class="col-12">
            <div class="form-check">
                <input wire:model.defer="itemMasterPassSecured" class="form-check-input" type="checkbox" id="secureByMasterPassword_update">

                <label class="form-check-label" for="secureByMasterPassword_update">
                    Secure By master password
                </label>
            </div>
        </div>

        <div class="col-12">
            <div class="form-check">
                <input wire:model.defer="itemIsFavourite" class="form-check-input" type="checkbox" value="" id="is_favourite_update">

                <label class="form-check-label" for="is_favourite_update">
                    Is Favourite
                </label>
            </div>
        </div>



        <br>
        <br>
        <button class="btn btn-primary" type="submit"> Update Item</button>

        <button class="btn btn-secondary" wire:click="goBack">Go Back</button>



    </form>
</div>
