<?php

namespace App\Imports;

use App\Models\Folder;
use App\Models\Item;
use App\Models\Type;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ItemsImport implements ToCollection, WithHeadingRow, WithValidation
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $collections)
    {
        $user = auth()->user();

        foreach ($collections as $collection) {
            try {
                Item::create([
                    'folder_id' => $this->createFolderAndReturnId($collection['folder']),
                    'is_favourite' => 0,
                    'type_id' => $this->getTypeIdByName($collection['type']),
                    'name' => $collection['name'],
                    'note' => $collection['note'],
                    'master_pass_secured' => ($collection['master_pass_secured']) ? 1 : 0,
                    'username' => $collection['login_username'],
                    'password' => $collection['login_password'],
                    'login_url' => $collection['login_url'],
                    'user_id' => $user->id,
                    'collection_id' => $user->collections->first()->id,
                    'organization_id' => $this->getOrganizationId(),
                    'deleted' => 0,
                    'in_trash' => 0,
                ]);
            } catch (\Exception $e) {
                // print_r($collection);
                // dd($e);
                Log::error($e->getMessage());
            }
        }
    }

    public function rules(): array
    {
        return [
            'folder' => 'required|string',
            'favourite' => 'boolean',
            'type' => 'required|exists:types,name',
            'name' => 'required|string|max:255',
            'note' => 'string|max:1000',
            'master_pass_secured' => 'boolean',
            'login_url' => 'required|string|max:255',
            'login_username' => 'required|string|max:255',
            'login_password' => 'required|string|max:255'
        ];
    }

    public function createFolderAndReturnId($folderName)
    {
        $user = auth()->user();
        $folder = $user->folders->where('name', $folderName)->first();
        if ($folder) {
            return $folder->id;
        } else {
            $folder = Folder::create([
                'name' => $folderName,
                'user_id' => $user->id,
            ]);
            return $folder->id;
        }
    }

    public function getTypeIdByName($name)
    {
        $type = Type::where('name', $name)->first();
        if ($type) {
            return $type->id;
        } else {
            // return first type id
            return Type::first()->id;
        }
    }

    public function getOrganizationId()
    {
        $user = auth()->user();
        $default_organization = $user->organizations->where('name', $user->email)->first();

        if ($default_organization) {
            return $default_organization->id;
        }
        return $user->organizations->first()->id;
    }
}
