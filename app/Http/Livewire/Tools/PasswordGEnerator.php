<?php

namespace App\Http\Livewire\Tools;

use Livewire\Component;

class PasswordGEnerator extends Component
{
    public $heading = "Generator";
    public function render()
    {
        return view('livewire.tools.password-g-enerator');
    }
}
