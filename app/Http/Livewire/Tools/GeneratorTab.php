<?php

namespace App\Http\Livewire\Tools;

use Livewire\Component;
use Faker\Factory as Faker;

class GeneratorTab extends Component
{

    public $passphraseWordsArray = [];


    public function mount()
    {
        $this->passphraseWordsArray = Faker::create()
            ->words(10000);

        // remove words with less than 4 characters
        $this->passphraseWordsArray = array_filter($this->passphraseWordsArray, function ($word) {
            return strlen($word) > 3;
        });

        $this->passphraseWordsArray = json_encode(array_values($this->passphraseWordsArray));
    }


    public function render()
    {
        return view('livewire.tools.generator-tab');
    }
}