<?php

namespace App\Http\Livewire\Vault;

use App\Models\Folder;
use App\Traits\IsOwnerTrait;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class FolderAccordion extends Component
{
    use IsOwnerTrait;
    public $folders = [];
    public $folder_name;

    public $selected_folder;

    public $edit_folder_name;
    public $edit_folder_id;

    protected $rules = [
        'folder_name' => 'required|min:2|max:15|regex:/^[\pL\s\-]+$/u',
    ];


    // protected $queryString = ['selected_folder'];
    public $user;

    public function mount($folders)
    {
        // dd($folders);
        $this->folders = $folders;

        $this->edit_folder_name = $folders->first()->name;
        $this->edit_folder_id = $folders->first()->id;
    }
    public function render()
    {
        return view('livewire.vault.folder-accordion');
    }

    public function createFolder()
    {
        $this->validate();
        $new_folder = Folder::create([
            'name' => $this->folder_name,
            'user_id' => $this->user->id
        ]);
        $this->folder_name = '';
        $this->updatePropertiesAndDispatchEvents('Folder Created', 'Folder created successfully.');
    }

    public function selectFolder(Folder $folder = null)
    {
        if ($folder == null) {
            $this->emitUp('folderSelected', 'no_folder');
        } else {
            $this->emitUp('folderSelected', $folder->id);
        }
    }

    public function editFolder()
    {
        $this->validate([
            'edit_folder_name' => 'required|string',
            'edit_folder_id' => 'required|integer'
        ]);

        try {
            $folder = Folder::findorFail($this->edit_folder_id);
            if (!$this->isTheOwner($folder->user_id)) {
                throw new \Exception("Unauthorized action", 403);
            }
            $folder->name = $this->edit_folder_name;
            $folder->save();
            $this->updatePropertiesAndDispatchEvents('Folder Edited', 'Folder edited successfully.');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function deleteFolder()
    {
        try {
            $folderCount = $this->user->folders()->count();

            $folder = Folder::findorFail($this->edit_folder_id);
            if (!$this->isTheOwner($folder->user_id)) {
                throw new \Exception("Unauthorized action", 403);
            }
            if ($folderCount == 1) {
                throw new \Exception("You can't delete the last folder", 403);
            }
            $folder->delete();
            $this->updatePropertiesAndDispatchEvents('Folder Deleted', 'Folder deleted successfully.');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function updatePropertiesAndDispatchEvents($event_label, $message = null)
    {
        $this->folders = Folder::where('user_id', $this->user->id)->get();
        $this->edit_folder_name = $this->folders->first()->name;
        $this->edit_folder_id = $this->folders->first()->id;
        $this->dispatchBrowserEvent('folderAccordion.folderUpdate', [
            'eventLabel' => $event_label,
            'message' => $message ?? ''
        ]);
        $this->emit('foldersUpdated');
    }
}
