<?php

namespace App\Http\Livewire\Vault;

use App\Models\Item;
use App\Models\Type;
use Livewire\Component;

class TrashedItems extends Component
{
    public $trashedItems;
    public $folders;
    public $organizations;
    public $itemTypes;
    public $selected_folder;

    public $selected_organization;
    public $selected_type;
    public $search;
    public $listeners = [
        'itemsUpdated' => 'itemsUpdated'
    ];
    public function mount()
    {
        $this->trashedItems = Item::where('in_trash', 1)->get();
    }

    public function resotreItem($id)
    {
        $item = Item::find($id);
        $item->in_trash = 0;
        $item->save();
        $this->emit('itemsUpdated');
    }

    public function deletePermamnently($id)
    {
        $item = Item::find($id);
        $item->delete();
        $this->emit('itemsUpdated');
    }

    public function itemsUpdated()
    {
        $this->trashedItems = Item::where('in_trash', 1)->get();
    }
    public function render()
    {
        return view('livewire.vault.trashed-items');
    }
}
