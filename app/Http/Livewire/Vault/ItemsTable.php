<?php

namespace App\Http\Livewire\Vault;

use App\Models\Folder;
use App\Models\Item;
use App\Models\Type;
use Exception;
use Illuminate\Support\Collection;
use Livewire\Component;
use App\Traits\IsOwnerTrait;
use Illuminate\Support\Facades\Log;

class ItemsTable extends Component
{
    use IsOwnerTrait;
    public $items;
    public $folders;
    public $organizations;

    public $masterPassItemId = null;
    public $masterPassMatched = null;
    public $showMasterPassPromt = true;
    public $listeners = [
        'itemsUpdated' => 'itemsUpdated',
        'foldersUpdated' => 'folderUpdated',
        'folderSelected_update_items_table' => 'folderSelected',
        'organizationSelected_update_items_table' => 'organizationSelected',
        'typeSelected_update_items_table' => 'typeSelected',
    ];
    public $itemTypes;
    public $selected_folder = null;

    public $selectedItems = [];
    public $folder_for_moving_items = null;
    public $selected_organization;
    public $selected_type;
    public $search;
    public $query;
    protected $queryString = [
        // 'selected_folder',
        // 'selected_organization',
        // 'selected_type',
        // 'search',
        // 'query'
    ];



    protected $rules = [
        'items.*.name' => 'required|string|min:2|max:15',
        'items.*.username' => 'required|string|min:2|max:15',
        'items.*.password' => 'required',
        'items.*.note' => 'required|string|max:500',
        'items.*.master_pass_secured' => 'required|boolean',
        'items.*.is_favourite' => 'required|boolean',
        'items.*.in_trash' => 'required|boolean',
        'items.*.type_id' => 'required|integer',
        'items.*.organization_id' => 'required|integer',
        'items.*.folder_id' => 'required|integer',
        'items.*.login' => 'required|integer',
        'items.*.collection_id' => 'required|integer',
        'items.*.user_id' => 'required|integer'
    ];

    public function mount()
    {
        if ($this->query != null) {
            $parts = explode('=', $this->query);

            if ($parts[0] == 'folder_id') {
                $this->selected_folder = $parts[1];
            } elseif ($parts[0] == 'organization_id') {
                $this->selected_organization = $parts[1];
            } elseif ($parts[0] == 'type_id') {
                $this->selected_type = $parts[1];
            }
        }
        $this->itemTypes = Type::all();
    }

    public function folderSelected($folder_id)
    {
        if ($folder_id == null) {
            $this->query = "folder_id=no_folder";
            $this->selected_folder = 'no_folder';
        } else {
            $this->query = "folder_id=" . $folder_id;
            $this->selected_folder = $folder_id;
        }

        $this->selected_organization = null;
        $this->selected_type = null;
        $this->removeAllSelectedItems();
    }

    public function organizationSelected($organization_id)
    {
        $this->query = "organization_id=" . $organization_id;
        $this->selected_folder = null;
        $this->selected_type = null;
        $this->selected_organization = $organization_id;
        $this->removeAllSelectedItems();
    }

    public function typeSelected($type_id)
    {
        $this->query = "type_id=" . $type_id;
        $this->selected_organization = null;
        $this->selected_folder = null;
        $this->selected_type = $type_id;
        $this->removeAllSelectedItems();
    }

    public function itemsUpdated($refreshQuery = false)
    {
        if ($refreshQuery) {
            $this->query = null;
            $this->selected_folder = null;
            $this->selected_organization = null;
            $this->selected_type = null;
        }
        $this->items = auth()->user()->items;
        $this->removeAllSelectedItems();
    }

    public function folderUpdated()
    {
        $this->folders = auth()->user()->folders;
        $this->removeAllSelectedItems();
    }

    public function render()
    {
        return view('livewire.vault.items-table');
    }

    public function updateItem()
    {
        $this->validate();
        try {
            foreach ($this->items as $item) {
                $item->save();
            }
            $this->dispatchBrowserEvent('itemUpdated');
            $this->emit('itemsUpdated');
        } catch (\Exception $exception) {
            $errorInfo = $exception;
            $this->dispatchBrowserEvent('itemUpdateFailed', $errorInfo);
        }
    }
    protected function deleteItemWithId($item_id)
    {
        try {
            $item = Item::findOrFail($item_id);
            if ($this->isTheOwner($item->user_id)) {
                $item->toTrash();
            } else {
                throw new Exception('Unauthorized Action', 403);
            }
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public function deleteItem($id)
    {
        try {
            $this->deleteItemWithId($id);
            $this->emit('itemsUpdated');
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    public function masterPassMatched()
    {
        // $this->masterPassMatched = $id;
    }

    protected function prepareForValidation($data): array
    {
        return collect($data)
            ->map(
                fn ($value) => $value instanceof Collection
                    ? $value->all()
                    : $value
            )
            ->all();
    }

    public function itemSelectedToUpdate($item)
    {
        $this->emitUp('itemSelectedToUpdate', $item);
    }

    public function moveItemsToFolder()
    {
        try {
            foreach ($this->selectedItems as $key => $item_id) {
                $item = Item::findOrFail($item_id);
                if ($this->isTheOwner($item->user_id)) {
                    $item->folder_id = $this->folder_for_moving_items;
                    $item->save();
                } else {
                    throw new Exception("Unaunthorized Action", 403);
                }
            }
            $this->selectedItems = [];
            $this->folder_for_moving_items = null;
            $this->emit('itemsUpdated');
            $this->dispatchBrowserEvent('itemsTable.selectedItemsMoved');
        } catch (\Exception $e) {
            Log::error($e);
            $this->dispatchBrowserEvent('itemsTable.somethingwentwrong');
        }
    }

    public function deleteSelectedItems()
    {
        try {
            foreach ($this->selectedItems as $key => $item_id) {

                $this->deleteItemWithId($item_id);
            }
            $this->selectedItems = [];
            $this->emit('itemsUpdated');
            $this->dispatchBrowserEvent('itemsTable.selectedItemsDeleted');
        } catch (\Exception $e) {
            $this->emit('itemsUpdated');
            $this->dispatchBrowserEvent('itemsTable.somethingwentwrong');
        }
    }

    public function removeAllSelectedItems()
    {
        $this->selectedItems = [];
    }
}
