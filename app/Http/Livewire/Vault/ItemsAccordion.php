<?php

namespace App\Http\Livewire\Vault;

use App\Models\Item;
use App\Models\Type;
use Livewire\Component;

class ItemsAccordion extends Component
{
    public $items = [];
    public $types = [];
    public function mount(){
        $this->types= Type::all();
    }
    public function render()
    {
        return view('livewire.vault.items-accordion');
    }

    public function selectItemType($type){
        $this->emitUp('typeSelected', $type['id']);
    }
}
