<?php

namespace App\Http\Livewire\Vault;

use App\Models\Item;
use App\Models\Type;
use Livewire\Component;
use Symfony\Contracts\Service\Attribute\Required;

class ItemEditView extends Component
{

    public $item;
    public $folders;
    public $organizations;
    public $types;

    // new Item properties
    public $itemType;
    public $itemName;
    public $itemFolder;
    public $itemUserName;
    public $itemLoginUrl;
    public $itemPassword;
    public $itemOrganization;
    public $itemNote;
    public $itemMasterPassSecured = false;
    public $itemIsFavourite = false;
    public $collectionId;

    protected $listeners = ['itemToEditUpdated'];
    public function render()
    {
        return view('livewire.vault.item-edit-view');
    }

    protected $rules = [
        'itemType' => 'required',
        'itemName' => 'required|min:3|max:255|regex:/^[\pL\s\-]+$/u',
        'itemFolder' => 'required',
        'itemUserName' => 'required|string|min:3|max:255',
        'itemPassword' => 'required|min:5',
        'itemOrganization' => 'required',
        'itemLoginUrl' => 'required|url',
    ];

    public function itemToEditUpdated($item_id)
    {

        $this->item = Item::find($item_id);
        $this->assignTolocal($this->item);
    }

    public function mount($item)
    {
        $this->item = $item;
        if ($this->item != null) {
            $this->assignTolocal($item);
        }
    }

    public function assignTolocal($item)
    {
        $this->itemType = $this->item->type_id;
        $this->itemName = $this->item->name;
        $this->itemFolder = $this->item->folder_id;
        $this->itemUserName = $this->item->username;
        $this->itemLoginUrl = $this->item->login_url;
        $this->itemPassword = $this->item->password;
        $this->itemOrganization = $this->item->organization_id;
        $this->itemNote = $this->item->note;
        $this->itemMasterPassSecured = $this->item->master_pass_secured;
        $this->itemIsFavourite = $this->item->is_favourite;
        $this->collectionId = $this->item->collection_id;

        $this->folders = $item->user->folders;
        $this->organizations = $item->user->organizations;
    }

    public function updateItemGoBackToList()
    {
        $this->validate();

        $this->item->type_id = $this->itemType;
        $this->item->name = $this->itemName;
        $this->item->folder_id = $this->itemFolder;
        $this->item->username = $this->itemUserName;
        $this->item->password = $this->itemPassword;
        $this->item->organization_id = $this->itemOrganization;
        $this->item->note = $this->itemNote;
        $this->item->master_pass_secured = $this->itemMasterPassSecured;
        $this->item->is_favourite = $this->itemIsFavourite;
        $this->item->collection_id = $this->collectionId;
        $this->item->login_url = $this->itemLoginUrl;

        $this->item->save();

        $this->emitUp('itemUpdated.exitEditView');
    }

    public function goBack()
    {
        $this->emitUp('itemUpdated.exitEditView');
    }
}
