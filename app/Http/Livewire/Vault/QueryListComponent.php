<?php

namespace App\Http\Livewire\Vault;

use App\Models\Folder;
use App\Models\Organization;
use App\Models\Type;
use Livewire\Component;

class QueryListComponent extends Component
{

    public $listeners = [
        'folderSelected_update_items_table' => 'updateFolderQueryListSection',
        'organizationSelected_update_items_table' => 'updateOrganisationQueryListSection',
        'typeSelected_update_items_table' => 'updateTypeQueryListSection',
    ];
    public $tag = '';
    public $query;

    public function mount($query)
    {
        $query_info = explode('=', $this->query);

        if ($query_info[0] == 'folder_id' && $query_info[1] != 'no_folder') {
            $this->tag = Folder::find($query_info[1])->name;
        } elseif ($query_info[0] == 'organization_id') {
            $this->tag = Organization::find($query_info[1])->name;
        } elseif ($query_info[0] == 'type_id') {
            $this->tag = Type::find($query_info[1])->name;
        } elseif ($query_info[0] == 'folder_id' && $query_info[1] == 'no_folder') {
            $this->tag = "No Folder";
        }
    }

    public function render()
    {
        return view('livewire.vault.query-list-component');
    }

    public function updateFolderQueryListSection($folder_id)
    {
        if ($folder_id == 'no_folder') {
            $this->tag = "No Folder";
        } else if (is_integer($folder_id)) {
            $this->tag = Folder::find($folder_id)->name;
        }
    }

    public function updateOrganisationQueryListSection($organization_id)
    {
        $this->tag = Organization::find($organization_id)->name;
    }

    public function updateTypeQueryListSection($type_id)
    {
        $this->tag = \App\Models\Type::find($type_id)->name;
    }

    public function removeQuery()
    {
        $this->tag = '';
        $this->emitUp('removeQuery', $this->query);
    }
}
