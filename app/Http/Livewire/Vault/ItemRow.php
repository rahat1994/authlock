<?php

namespace App\Http\Livewire\Vault;

use App\Models\Item;
use Livewire\Component;

class ItemRow extends Component
{
    public Item $item;
    public $index;
    public $itemTypes;

    public $folders;

    public $organizations;

    // new Item properties
    public $itemType;
    public $itemName;
    public $itemFolder;
    public $itemUserName;
    public $itemPassword;
    public $itemOrganization;
    public $itemNote;
    public $itemMasterPassSecured = false;
    public $itemIsFavourite = false;
    public $collectionId;

    public function mount(){
        $this->itemType = $this->item->type_id;
        $this->itemName = $this->item->name;
        $this->itemFolder = $this->item->folder_id;
        $this->itemUserName = $this->item->username;
        $this->itemPassword = $this->item->password;
        $this->itemOrganization = $this->item->organization_id;
        $this->itemNote = $this->item->note;
        $this->itemMasterPassSecured = $this->item->master_pass_secured;
        $this->itemIsFavourite = $this->item->is_favourite;
        $this->collectionId = $this->item->collection_id;
    }

    protected $rules = [
        'item.name' => 'required',
        'item.username' => 'required',
        'item.note' => 'required',
        'item.master_pass_secured' => 'required',
        'item.is_favourite' => 'required',
        'item.collection_id' => 'required',
        'item.password' => 'required',
        'item.organization_id' => 'required',
        'item.folder_id' => 'required',
        'item.type_id' => 'required',
    ];

    public function render()
    {
        return view('livewire.vault.item-row');
    }

    // generate documentation for this method

    public function updateItem(){
        $this->validate();
        $this->item->save();
    }
}
