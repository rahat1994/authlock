<?php

namespace App\Http\Livewire\Vault;

use App\Models\Folder;
use App\Models\Item;
use App\Models\Organization;
use App\Models\Type;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class VaultView extends Component
{

    public $heading = 'All Items';
    public $user;
    public $types;
    public $selected_folder = null;
    public $selected_organization = null;
    public $selected_type = null;
    public $search = null;
    protected $queryString = ['selected_folder' => ['except' => 'null'], 'selected_organization', 'selected_type', 'search', 'query'];

    public $folders = [];
    public $organizations = [];
    public $items = [];
    public $trashItems = [];
    public $query = null;
    public $itemEditMode = false;

    public $itemToEdit;
    protected $listeners = [
        'folderSelected',
        'organizationSelected',
        'typeSelected',
        'itemSelectedToUpdate',
        'itemUpdated.exitEditView' => 'exitEditView',
        'removeQuery'
    ];

    public function mount()
    {

        $data = Auth::user()->load('folders', 'items', 'organizations');
        $this->user = $data;

        $this->folders = $data->folders;
        $this->organizations = $data->organizations;
        $this->items = $data->items;
        $this->types = Type::all();
    }
    public function render()
    {
        return view('livewire.vault.vault-view');
    }

    public function folderSelected($folder_id)
    {
        if ($folder_id == null || $folder_id == 'no_folder') {
            $this->query = "folder_id=no_folder";
            $this->heading = 'All Items';
            $this->emit('folderSelected_update_items_table', 'no_folder');
            $this->emit('headingUpdated', $this->heading);
            return;
        } else {
            $this->query = "folder_id=" . $folder_id;
            $this->heading = Folder::find($folder_id)->name;

            $this->emit('folderSelected_update_items_table', $folder_id);
            $this->emit('headingUpdated', $this->heading);
        }
    }
    public function organizationSelected($organization_id)
    {
        $this->query = "organization_id=" . $organization_id;
        $organization = Organization::find($organization_id);
        $this->heading = 'All Items (' . $organization->name . ')';

        $this->emit('organizationSelected_update_items_table', $organization_id);
        $this->emit('headingUpdated', $this->heading);
    }

    public function typeSelected($typeId)
    {
        $this->query = "type_id=" . $typeId;
        $type = Type::find($typeId);
        $this->heading = 'All Items (' . $type->name . ')';

        $this->emit('typeSelected_update_items_table', $typeId);
        $this->emit('headingUpdated', $this->heading);
    }

    public function itemSelectedToUpdate($itemId)
    {
        $this->itemEditMode = true;
        $this->itemToEdit = Item::find($itemId);
        $this->emit('itemToEditUpdated', $itemId);
    }

    public function exitEditView()
    {
        $this->emit('itemsUpdated');
        $this->itemEditMode = false;
    }

    public function removeQuery()
    {
        $this->query = null;
        $this->heading = 'All Items';
        $this->emit('headingUpdated', $this->heading);
        $this->emit('itemsUpdated', true);
    }
}
