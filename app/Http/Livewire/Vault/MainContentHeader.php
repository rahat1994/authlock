<?php

namespace App\Http\Livewire\Vault;

use App\Models\Type;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Illuminate\Support\Facades\Log;

class MainContentHeader extends Component
{
    public $heading;
    public $itemTypes;
    public $folders;
    public $organizations;
    public $user;

    // new Item properties
    public $itemType;
    public $itemName;
    public $itemFolder;
    public $itemUserName;
    public $itemPassword;
    public $itemOrganization;
    public $itemNote = '';
    public $item;
    public $itemMasterPassSecured = false;
    public $itemIsFavourite = false;
    public $collectionId;

    public $loginUrl;
    public $listeners = ['organizationsUpdated' => 'organizationsUpdated', 'foldersUpdated' => 'foldersUpdated', 'headingUpdated'];

    public function mount()
    {
        $this->organizations = $this->user->organizations;
        $this->itemTypes = Type::all();
        $collection = $this->user->collections;
        $this->collectionId = $collection->first()->id;
        $this->itemFolder = $this->user->folders->first()->id;
        $this->itemOrganization = $this->user->organizations->first()->id;
        $this->itemType = $this->itemTypes->first()->id;
    }

    public function render()
    {
        return view('livewire.vault.main-content-header');
    }

    public function createItem()
    {
        $this->validate([
            'itemType' => 'required',
            'itemName' => 'required|min:3|max:255|regex:/^[\pL\s\-]+$/u',
            'itemFolder' => 'required',
            'itemUserName' => 'required|string|min:3|max:255',
            'itemPassword' => 'required|min:5',
            'itemOrganization' => 'required',
            'loginUrl' => 'required|url',
        ]);

        try {
            $this->item = $this->user->items()->create([
                'name' => $this->itemName,
                'username' => $this->itemUserName,
                'note' => $this->itemNote,
                'master_pass_secured' => $this->itemMasterPassSecured ? 1 : 0,
                'is_favourite' => $this->itemIsFavourite ? 1 : 0,
                'in_trash' => 0,
                'deleted' => 0,
                'type_id' => $this->itemType,
                'organization_id' => $this->itemOrganization,
                'folder_id' => $this->itemFolder,
                'collection_id' => $this->collectionId,
                'password' => $this->itemPassword,
                'login_url' => $this->loginUrl,
            ]);

            $this->reset(['itemName', 'itemUserName', 'itemPassword', 'itemNote', 'itemMasterPassSecured', 'itemIsFavourite', 'loginUrl']);

            $this->itemFolder = $this->user->folders->first()->id;
            $this->itemOrganization = $this->user->organizations->first()->id;
            $this->itemType = $this->itemTypes->first()->id;

            $this->dispatchBrowserEvent('itemCreated', $this->item->id);
            $this->emit('itemsUpdated');
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            // dd($errorInfo);
            Log::error($errorInfo);
            $this->dispatchBrowserEvent('itemCreationFailed', $errorInfo);
        }
    }

    public function organizationsUpdated()
    {
        $this->organizations = $this->user->organizations;
    }

    public function foldersUpdated()
    {
        $this->folders = $this->user->folders;
    }

    public function headingUpdated($heading)
    {
        $this->heading = $heading;
    }
}
