<?php

namespace App\Http\Livewire\Vault;

use App\Models\Organization;
use Livewire\Component;

class OrganizationsAccoridion extends Component
{
    public $organizations = [];

    public $organizationName = '';
    public $billingAddress = '';
    public $user;
    public $organization;

    protected $rules = [
        'organizationName' => 'required|regex:/^[\pL\s\-]+$/u',
        'billingAddress' => 'required|email'
    ];

    public function render()
    {
        return view('livewire.vault.organizations-accoridion');
    }

    public function createOrganization()
    {
        $this->validate();
        try {
            $this->organization = $this->user->organizations()->create([
                'name' => $this->organizationName,
                'billing_email' => $this->billingAddress,
                'user_id' => $this->user->id
            ]);
            $this->organizations = auth()->user()->organizations;
            $this->dispatchBrowserEvent('organizationCreated', $this->organization->id);
            $this->emit('organizationsUpdated');
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            $this->dispatchBrowserEvent('organizationCreationFailed', $errorInfo);
        }

        $this->organizationName = '';
        $this->billingAddress = '';
    }

    public function selectOrganization(Organization $organization)
    {
        $this->emitUp('organizationSelected', $organization->id);
    }
}
