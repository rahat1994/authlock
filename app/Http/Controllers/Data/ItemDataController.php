<?php

namespace App\Http\Controllers\Data;

use App\Exports\ItemExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Data\Item\ItemExportRequest;
use App\Http\Requests\Data\Item\ItemImportRequest;
use App\Imports\ItemsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;

class ItemDataController extends Controller
{
    public function export(ItemExportRequest $request)
    {
        $format = $request->input('format');

        try {
            if ($format === 'xlsx') {
                return Excel::download(new ItemExport, 'item.xlsx', \Maatwebsite\Excel\Excel::XLSX);
            } elseif ($format === 'csv') {
                return Excel::download(new ItemExport, 'item.csv', \Maatwebsite\Excel\Excel::CSV);
            } elseif ($format === 'pdf') {
                return Excel::download(new ItemExport, 'item.pdf', \Maatwebsite\Excel\Excel::MPDF);
            } else {
                return redirect()->back()->with('error', 'Invalid format');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function import(ItemImportRequest $request)
    {

        $headings = (new HeadingRowImport)->toArray($request->import_file);
        $headings_expected = [
            0 => "folder",
            1 => "favourite",
            2 => "type",
            3 => "name",
            4 => "note",
            5 => "master_pass_secured",
            6 => "login_url",
            7 => "login_username",
            8 => "login_password"
        ];

        if ($headings[0][0] !== $headings_expected) {
            return redirect()->back()->with('error', 'Invalid headings');
        }

        Excel::import(new ItemsImport, $request->file('import_file'), null, \Maatwebsite\Excel\Excel::CSV);
        return redirect('/dashboard')->with('success', 'All good!');
    }
}
