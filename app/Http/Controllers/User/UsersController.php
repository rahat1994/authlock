<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserExport;

class UsersController extends Controller
{
    public function export()
    {
        return Excel::download(new UserExport, 'users.xlsx');
    }
}
