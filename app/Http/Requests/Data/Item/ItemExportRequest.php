<?php

namespace App\Http\Requests\Data\Item;

use Illuminate\Foundation\Http\FormRequest;

class ItemExportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'format' => 'required|string|in:xlsx,csv,pdf',
        ];
    }
}
