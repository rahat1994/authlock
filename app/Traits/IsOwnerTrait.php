<?php

namespace App\Traits;


trait IsOwnerTrait
{
    public function isTheOwner($user_id)
    {
        return auth()->user()->id === $user_id;
    }
}
