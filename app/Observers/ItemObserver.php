<?php

namespace App\Observers;

use App\Models\Item;
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;

class ItemObserver
{
    /**
     * Handle the Item "created" event.
     *
     * @param  \App\Models\Item  $item
     * @return void
     */
    public function created(Item $item)
    {
        //
    }

    /**
     * Handle the Item "creating" event.
     *
     * @param  \App\Models\Item  $item
     * @return void
     */
    public function creating(Item $item)
    {
        $key = Key::createNewRandomKey();
        $item->key = $key->saveToAsciiSafeString();

        // encrypt the pass using generated key
        $item->password = Crypto::encrypt($item->password, $key);
    }

    /**
     * Handle the Item "retrived" event.
     *
     * @param  \App\Models\Item  $item
     * @return void
     */
    public function retrieved(Item $item)
    {
        $key = Key::loadFromAsciiSafeString($item->key);
        $item->password = Crypto::decrypt($item->password, $key);
    }

    /**
     * Handle the Item "updated" event.
     *
     * @param  \App\Models\Item  $item
     * @return void
     */
    public function updating(Item $item)
    {
        $key = Key::createNewRandomKey();
        $item->key = $key->saveToAsciiSafeString();

        // encrypt the pass using generated key
        $item->password = Crypto::encrypt($item->password, $key);
    }

    /**
     * Handle the Item "deleted" event.
     *
     * @param  \App\Models\Item  $item
     * @return void
     */
    public function deleted(Item $item)
    {
        //
    }

    /**
     * Handle the Item "restored" event.
     *
     * @param  \App\Models\Item  $item
     * @return void
     */
    public function restored(Item $item)
    {
        //
    }

    /**
     * Handle the Item "force deleted" event.
     *
     * @param  \App\Models\Item  $item
     * @return void
     */
    public function forceDeleted(Item $item)
    {
        //
    }
}
