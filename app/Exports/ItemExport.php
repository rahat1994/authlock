<?php

namespace App\Exports;

use App\Models\Item;
use Illuminate\Support\Facades\Date;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ItemExport implements FromCollection, WithHeadings, WithMapping
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $user = auth()->user();

        return Item::where('user_id', $user->id)->with('folder:id,name', 'type:id,name')->get();
    }

    public function headings(): array
    {
        return [
            'folder',
            'favourite',
            'type',
            'name',
            'note',
            'master_pass_secured',
            'login_url',
            'login_username',
            'login_password',
        ];
    }

    /**
     * @var Item $item
     */
    public function map($item): array
    {
        return [
            is_null($item->folder) ? "Exported Folder" : $item->folder->name,
            ($item->is_favourite) ? "true" : "false",
            $item->type->name,
            $item->name,
            $item->note,
            ($item->master_pass_secured) ? "true" : "false",
            $item->login_url,
            $item->username,
            $item->password,
        ];
    }
}
