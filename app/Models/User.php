<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function organizations(){

        return $this->belongsToMany(Organization::class, 'user_organizations');
    }

    public function folders(){
        return $this->hasMany(Folder::class);
    }

    public function collections(){
        return $this->hasMany(Collection::class);
    }

    public function Items(){
        return $this->hasMany(Item::class);
    }

    // create documentation for the method

    public static function boot() {
        static::created(function(User $user) {
           $organization =  $user->organizations()->create([
                'name' => $user->email,
                'billing_email' => $user->email,
                'user_id' => $user->id
            ]);

            $user->collections()->create([
                'name' => 'Default Collection',
                'organization_id' => $organization->id,
                'user_id' => $user->id
            ]);

            $user->folders()->create([
                'name' => 'Default Folder',
                'user_id' => $user->id
            ]);
        });

        parent::boot();
    }
}
