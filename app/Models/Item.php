<?php

namespace App\Models;

use App\Observers\ItemObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Summary of Item
 */
class Item extends Model
{
    use HasFactory;

    public $fillable = [
        'name',
        'username',
        'password',
        'note',
        'master_pass_secured',
        'is_favourite',
        'in_trash',
        'deleted',
        'type_id',
        'folder_id',
        'organization_id',
        'collection_id',
        'user_id',
        'login_url'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function folder()
    {
        return $this->belongsTo(Folder::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function toTrash()
    {
        $this->in_trash = 1;
        return $this->save();
    }

    /**
     * Summary of boot
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::observe(ItemObserver::class);
    }
}
